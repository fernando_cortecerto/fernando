﻿Imports iTextSharp.text.pdf
Imports iTextSharp.text
Public Class Principal

    Public Sub GeraEtiquetas(CaminhoDoArquivo As String, MargemEsquerdaMM As Double, MargemDireitaMM As Double, MargemSuperiorMM As Double, MargemInferiorMM As Double, NúmeroDeLinhas As Single, NúmeroDeColunas As Single,
                             LarguraDaEtiquetaMM As Double, AlturaDaEtiquetaMM As Double, ConteúdoEtiquetas As String, DistanciaVerticalMM As Double, DistanciaHorizontalMM As Double, TipoDePapel As Long,
                             ContornoFolha As Double, ContornoEtiqueta As Double)
        'A4 (210 x 297)
        'A4356 25,4 x 63,5. Etiquetas por folha 33
        'Código Pimaco              Margem Superior   Margem Lateral  Densidade Vertical    Densidade horizontal	 Altura da Etiqueta    Largura da Etiqueta    Etiqueta por Linha   Linha por  Página
        'A4056/A4256/A4356/A4056R	0,88	          0,72	          2,54	                6,61	                 2,54	               6,35	                 3	                  11
        'Carta (215,9 x 279,4)
        '6280 25,4 x 63,5. Etiquetas por folha 33
        'Código Pimaco              Margem Superior   Margem Lateral  Densidade Vertical    Densidade horizontal	 Altura da Etiqueta    Largura da Etiqueta    Etiqueta por Linha   Linha por  Página
        '6080/6180/ 6280/62580	    1,27	          0,48	          2,54	                6,98	                 2,54	               6,67	                  3	                   10


        Dim PixelScala As Double = 2.833 '2.7'3.779527559
        Dim LinhasConteúdo As Object = Split(ConteúdoEtiquetas, vbCrLf)
        Dim ColunaConteúdo As Object
        Dim FS As New System.IO.FileStream(CaminhoDoArquivo, System.IO.FileMode.Create)
        'Dim DOC As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, MargemEsquerdaMM, MargemDireitaMM, MargemSuperiorMM, MargemInferiorMM)
        Dim DOC As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 0, 0, 0, 0)
        Select Case TipoDePapel
            Case 1 'A4
                DOC = New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 0, 0, 0, 0)
            Case 2 'Carta
                DOC = New iTextSharp.text.Document(iTextSharp.text.PageSize.LETTER, 0, 0, 0, 0)
        End Select
        Dim PdfW As iTextSharp.text.pdf.PdfWriter = iTextSharp.text.pdf.PdfWriter.GetInstance(DOC, FS)
        Dim img As iTextSharp.text.Image
        DOC.Open()
        Dim PdfCB As iTextSharp.text.pdf.PdfContentByte = PdfW.DirectContent
        Dim ContornoPágina As iTextSharp.text.pdf.PdfContentByte = PdfW.DirectContent
        Dim Barcode As iTextSharp.text.pdf.Barcode128 = New Barcode128()
        Dim Barcode39 As iTextSharp.text.pdf.Barcode39 = New Barcode39()

        Dim BS As iTextSharp.text.pdf.BaseFont = iTextSharp.text.pdf.BaseFont.CreateFont(iTextSharp.text.pdf.BaseFont.COURIER, iTextSharp.text.pdf.BaseFont.CP1252, False)
        'Dim BS As iTextSharp.text.pdf.BaseFont = iTextSharp.text.pdf.BaseFont.CreateFont(iTextSharp.text.pdf.BaseFont.HELVETICA, iTextSharp.text.pdf.BaseFont.CP1252, False)
        'Converte largura e altura da etiqueta de mm em pixels '1 mm = 3.779528
        Dim LarguraEtiquetaPixel As Double = LarguraDaEtiquetaMM * PixelScala
        Dim AlturaEtiquetaPixel As Double = AlturaDaEtiquetaMM * PixelScala
        Dim DistanciaHorizontalPixel As Double = DistanciaHorizontalMM * PixelScala
        Dim DistanciaVerticalPixel As Double = DistanciaVerticalMM * PixelScala
        'Define tamanho em pixels do A4 210 × 297
        Dim LarguraDoPapelPixel As Double = 210 * PixelScala
        Dim AlturaDoPapelPixel As Double = 297 * PixelScala
        Select Case TipoDePapel
            Case 1 'A4
                LarguraDoPapelPixel = 210 * PixelScala
                AlturaDoPapelPixel = 297 * PixelScala
            Case 2 'Carta '215,9 x 279,4
                LarguraDoPapelPixel = 215.9 * PixelScala
                AlturaDoPapelPixel = 279.4 * PixelScala
        End Select

        'Define Margens
        Dim MargemEsquerdaPixel As Double = MargemEsquerdaMM * PixelScala
        Dim MargemDireitaPixel As Double = MargemDireitaMM * PixelScala
        Dim MargemSuperiorPixel As Double = MargemSuperiorMM * PixelScala
        Dim MargemInferiorPixel As Double = MargemInferiorMM * PixelScala
        'Tamanho do desenho da peça
        Dim LarguraDaPeçaDesenho As Double = 20
        Dim AlturaDaPeçaDesenho As Double = 20
        'Conta Etiquetas
        Dim SplitaLinhasEtiquetas As Object
        SplitaLinhasEtiquetas = Split(ConteúdoEtiquetas, vbCrLf)
        Dim NúmeroDeEtiquetas As Double = UBound(SplitaLinhasEtiquetas)
        Dim NúmeroDePáginas As Long = Math.Ceiling(NúmeroDeEtiquetas / (NúmeroDeLinhas * NúmeroDeColunas))
        Dim TotalEtiquetasPorPágina As Long = NúmeroDeLinhas * NúmeroDeColunas
        Dim EtiquetasPorPágina As Long = 0
        Dim ContadorDeColunas As Long = 0
        Dim XEtiq As Double = MargemEsquerdaPixel
        Dim YEtiq As Double = AlturaDoPapelPixel - (AlturaEtiquetaPixel + MargemSuperiorPixel)
        Dim DivideDescrição As Object


        For i As Long = 0 To NúmeroDeEtiquetas - 1
            ColunaConteúdo = Split(LinhasConteúdo(i), ";")
            DivideDescrição = Split(ColunaConteúdo(8) & "@@@@@@@", "@")
            EtiquetasPorPágina = EtiquetasPorPágina + 1
            ContadorDeColunas = ContadorDeColunas + 1
            If ContadorDeColunas > NúmeroDeColunas Then
                YEtiq = YEtiq - (AlturaEtiquetaPixel + DistanciaVerticalPixel)
                XEtiq = MargemEsquerdaPixel
                ContadorDeColunas = 1
            End If
            'Contorno da página 
            Select Case ContornoFolha
                Case True
                    PdfCB.SetRGBColorStroke(0, 0, 0) 'Preto    
                    PdfCB.SetLineDash(1)
                    PdfCB.SetLineWidth(0.5)
                    PdfCB.Rectangle(0, 0, LarguraDoPapelPixel, AlturaDoPapelPixel)
                    PdfCB.Stroke()
            End Select
            'Contorno da etiqueta Retangulo
            Select Case ContornoEtiqueta
                Case True
                    PdfCB.SetRGBColorStroke(255, 0, 0) 'RED
                    PdfCB.SetLineDash(1)
                    PdfCB.SetLineWidth(0.5)
                    PdfCB.Rectangle(XEtiq, YEtiq, LarguraEtiquetaPixel, AlturaEtiquetaPixel)
                    PdfCB.Stroke()
            End Select
            'Desenho da peça
            PdfCB.SetRGBColorStroke(0, 0, 0) '
            PdfCB.SetLineWidth(0.5)
            PdfCB.SetLineDash(4, 1, 1)
            PdfCB.Rectangle(XEtiq + LarguraEtiquetaPixel - 28, YEtiq + (AlturaEtiquetaPixel - (AlturaDaPeçaDesenho + 8)), LarguraDaPeçaDesenho, AlturaDaPeçaDesenho)
            PdfCB.Stroke()
            'Desenho da fita superior na peça
            If InStr(ColunaConteúdo(8), "@T") > 0 Then
                PdfCB.SetRGBColorStroke(0, 0, 0) '
                PdfCB.SetLineWidth(2)
                PdfCB.SetLineDash(1)
                PdfCB.MoveTo(XEtiq + LarguraEtiquetaPixel - 28, YEtiq + (AlturaEtiquetaPixel - (8)))
                PdfCB.LineTo((XEtiq + LarguraEtiquetaPixel - 28) + LarguraDaPeçaDesenho, (YEtiq + (AlturaEtiquetaPixel - (8))))
                PdfCB.Stroke()
            End If
            'Desenho da fita inferior na peça
            If InStr(ColunaConteúdo(8), "@B") > 0 Then
                PdfCB.SetRGBColorStroke(0, 0, 0) '
                PdfCB.SetLineWidth(2)
                PdfCB.SetLineDash(1)
                PdfCB.MoveTo(XEtiq + LarguraEtiquetaPixel - 28, YEtiq + (AlturaEtiquetaPixel - (AlturaDaPeçaDesenho + 8)))
                PdfCB.LineTo((XEtiq + LarguraEtiquetaPixel - 28) + LarguraDaPeçaDesenho, (YEtiq + (AlturaEtiquetaPixel - (AlturaDaPeçaDesenho + 8))))
                PdfCB.Stroke()
            End If
            'Desenho da fita esquerda na peça
            If InStr(ColunaConteúdo(8), "@L") > 0 Then
                PdfCB.SetRGBColorStroke(0, 0, 0) '
                PdfCB.SetLineWidth(2)
                PdfCB.SetLineDash(1)
                PdfCB.MoveTo(XEtiq + LarguraEtiquetaPixel - 28, YEtiq + (AlturaEtiquetaPixel - (8)))
                PdfCB.LineTo((XEtiq + LarguraEtiquetaPixel - 28), (YEtiq + (AlturaEtiquetaPixel - (AlturaDaPeçaDesenho + 8))))
                PdfCB.Stroke()
            End If
            'Desenho da fita direita na peça
            If InStr(ColunaConteúdo(8), "@L") > 0 Then
                PdfCB.SetRGBColorStroke(0, 0, 0) '
                PdfCB.SetLineWidth(2)
                PdfCB.SetLineDash(1)
                PdfCB.MoveTo((XEtiq + LarguraEtiquetaPixel - 28) + LarguraDaPeçaDesenho, YEtiq + (AlturaEtiquetaPixel - (8)))
                PdfCB.LineTo((XEtiq + LarguraEtiquetaPixel - 28) + LarguraDaPeçaDesenho, (YEtiq + (AlturaEtiquetaPixel - (AlturaDaPeçaDesenho + 8))))
                PdfCB.Stroke()
            End If
            'Cliente - Texto label
            With PdfCB
                .BeginText()
                .SetFontAndSize(BS, 8)
                .SetTextMatrix(XEtiq + 6, YEtiq + (AlturaEtiquetaPixel - 12))
                .ShowText("Cliente:")
                .EndText()
            End With
            'Cliente - Texto conteúdo
            With PdfCB
                .BeginText()
                .SetFontAndSize(BS, 8)
                .SetTextMatrix(XEtiq + 46, YEtiq + (AlturaEtiquetaPixel - 12))
                .ShowText(Strings.Left(ColunaConteúdo(9), 21))
                .EndText()
            End With
            'Peça: - Texto Label
            With PdfCB
                .BeginText()
                .SetFontAndSize(BS, 8)
                .SetTextMatrix(XEtiq + 6, YEtiq + (AlturaEtiquetaPixel - 22))
                .ShowText("Peça:")
                .EndText()
            End With
            'Peça: - Texto Conteúdo
            With PdfCB
                .BeginText()
                .SetFontAndSize(BS, 8)
                .SetTextMatrix(XEtiq + 30, YEtiq + (AlturaEtiquetaPixel - 22))
                .ShowText(Strings.Left(ColunaConteúdo(1) & " x " & ColunaConteúdo(2) & " - " & DivideDescrição(8), 23))
                .EndText()
            End With
            'Fita: - Texto Label
            With PdfCB
                .BeginText()
                .SetFontAndSize(BS, 8)
                .SetTextMatrix(XEtiq + 6, YEtiq + (AlturaEtiquetaPixel - 32))
                .ShowText("Fita:")
                .EndText()
            End With
            'Fita: - Texto Conteúdo
            With PdfCB
                .BeginText()
                .SetFontAndSize(BS, 8)
                .SetTextMatrix(XEtiq + 6, YEtiq + (AlturaEtiquetaPixel - 32))
                .ShowText("")
                .EndText()
            End With
            'Chapa: - Texto Label
            With PdfCB
                .BeginText()
                .SetFontAndSize(BS, 8)
                .SetTextMatrix(XEtiq + 6, YEtiq + (AlturaEtiquetaPixel - 42))
                .ShowText("Chapa:")
                .EndText()
            End With
            'Chapa: - Texto Conteúdo
            With PdfCB
                .BeginText()
                .SetFontAndSize(BS, 8)
                .SetTextMatrix(XEtiq + 35, YEtiq + (AlturaEtiquetaPixel - 42))
                .ShowText(Strings.Left(ColunaConteúdo(10), 26))

                .EndText()
            End With
            'Código de barras
            With Barcode
                .TextAlignment = Element.ALIGN_LEFT
                .Code = ColunaConteúdo(0) & "123456789"
                .StartStopText = False
                .CodeType = iTextSharp.text.pdf.Barcode128.EAN13
                .Extended = True
                .BarHeight = 7
                .TextAlignment = ContentAlignment.BottomCenter
            End With
            img = Barcode.CreateImageWithBarcode(PdfCB, Nothing, Nothing)
            img.SetAbsolutePosition((XEtiq + (LarguraEtiquetaPixel / 2)) - (img.Width / 2), YEtiq + (AlturaEtiquetaPixel - 66))
            PdfCB.AddImage(img)
            'Pula pra próxima
            XEtiq = XEtiq + LarguraEtiquetaPixel + DistanciaHorizontalPixel
            If EtiquetasPorPágina = TotalEtiquetasPorPágina Then
                'Nova página 
                DOC.NewPage()
                EtiquetasPorPágina = 0
                ContadorDeColunas = 0
                YEtiq = AlturaDoPapelPixel - (AlturaEtiquetaPixel + MargemSuperiorPixel)
                XEtiq = MargemEsquerdaPixel
            End If
        Next
        DOC.Close()
        FS.Close()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        'Arquivo CSV separado por ponto e vírgula
        '0 - Código da peça
        '1 - Largura da peça
        '2 - Altura da peça
        '3 - Descrição da fita esquerda da peça
        '4 - Descrição da fita direita da peça
        '5 - Descrição da fita superior da peça
        '6 - Descrição da fita inferior da peça
        '7 - Usinagem da peça
        '8 - Descrição da peça
        '9 - Descrição do Projeto
        '10 - Descrição do Material
        '11 - Código de barras (Reservar)
        Dim Conteúdo As String = "1;200;300;BRANCO PVC .45;BRANCO PVC .45;BRANCO PVC .45;BRANCO PVC .45;2 furos de dobradiça;Lateral;Prj 11;MDF 18mm Branco;123456789" & vbCrLf
        Conteúdo = Conteúdo & "21;700;800;BRANCO PVC .45;;BRANCO PVC .45;BRANCO PVC .45;2 furos de dobradiça;Lateral;Prj 11;MDF 18mm Branco;123456789" & vbCrLf

        'Dim Conteúdo As String = "1;AROLDO- SONIA;;??;??;??;??;??;??;??;??;5;1;735;70;12677;15;MDF BRANCO 15MM 2F. 2,75X1,85;C:\corte_certo_plus\PEDLABEL\PLA00010_00005.bmp; @B8  @T8  ;;BET;;;;;1;3;AROLDO;10;;;BET;;;;;AROLDO;;;;;;4227072;0;42;735;70;;;;-;;;;;;;;;;;;;;;;;12677;" & vbCrLf
        'Conteúdo = Conteúdo & "2;AROLDO- SONIA;;??;??;??;??;??;??;??;??;13;1;336;70;12677;15;MDF BRANCO 15MM 2F. 2,75X1,85;C:\corte_certo_plus\PEDLABEL\PLA00010_00013.bmp; @B8  @T8  @L8 @R8 ;;BET;;;;;1;3;AROLDO;10;;;BET;;;;;AROLDO;;;;;;4227072;0;42;336;70;;;;-;;;;;;;;;;;;;;;;;12677;" & vbCrLf
        'Conteúdo = Conteúdo & "3;AROLDO- SONIA;;??;??;??;??;??;??;??;??;12;1;335;70;12677;15;MDF BRANCO 15MM 2F. 2,75X1,85;C:\corte_certo_plus\PEDLABEL\PLA00010_00012.bmp; @B8  @T8  @L8 @R8 ;;BET;;;;;1;3;AROLDO;10;;;BET;;;;;AROLDO;;;;;;4227072;0;42;335;70;;;;-;;;;;;;;;;;;;;;;;12677;" & vbCrLf
        'Conteúdo = Conteúdo & "4;AROLDO- SONIA;;??;??;??;??;??;??;??;??;5;1;735;70;12677;15;MDF BRANCO 15MM 2F. 2,75X1,85;C:\corte_certo_plus\PEDLABEL\PLA00010_00005.bmp; @B8  @T8  ;;BET;;;;;1;3;AROLDO;10;;;BET;;;;;AROLDO;;;;;;4227072;0;42;735;70;;;;-;;;;;;;;;;;;;;;;;12677;" & vbCrLf
        'Conteúdo = Conteúdo & "5;AROLDO- SONIA;;??;??;??;??;??;??;??;??;13;1;336;70;12677;15;MDF BRANCO 15MM 2F. 2,75X1,85;C:\corte_certo_plus\PEDLABEL\PLA00010_00013.bmp; @B8  @T8  @L8 @R8 ;;BET;;;;;1;3;AROLDO;10;;;BET;;;;;AROLDO;;;;;;4227072;0;42;336;70;;;;-;;;;;;;;;;;;;;;;;12677;" & vbCrLf
        'Conteúdo = Conteúdo & "6;AROLDO- SONIA;;??;??;??;??;??;??;??;??;12;1;335;70;12677;15;MDF BRANCO 15MM 2F. 2,75X1,85;C:\corte_certo_plus\PEDLABEL\PLA00010_00012.bmp; @B8  @T8  @L8 @R8 ;;BET;;;;;1;3;AROLDO;10;;;BET;;;;;AROLDO;;;;;;4227072;0;42;335;70;;;;-;;;;;;;;;;;;;;;;;12677;" & vbCrLf
        'Conteúdo = Conteúdo & "7;AROLDO- SONIA;;??;??;??;??;??;??;??;??;5;1;735;70;12677;15;MDF BRANCO 15MM 2F. 2,75X1,85;C:\corte_certo_plus\PEDLABEL\PLA00010_00005.bmp; @B8  @T8  ;;BET;;;;;1;3;AROLDO;10;;;BET;;;;;AROLDO;;;;;;4227072;0;42;735;70;;;;-;;;;;;;;;;;;;;;;;12677;" & vbCrLf
        'Conteúdo = Conteúdo & "8;AROLDO- SONIA;;??;??;??;??;??;??;??;??;13;1;336;70;12677;15;MDF BRANCO 15MM 2F. 2,75X1,85;C:\corte_certo_plus\PEDLABEL\PLA00010_00013.bmp; @B8  @T8  @L8 @R8 ;;BET;;;;;1;3;AROLDO;10;;;BET;;;;;AROLDO;;;;;;4227072;0;42;336;70;;;;-;;;;;;;;;;;;;;;;;12677;" & vbCrLf
        'Conteúdo = Conteúdo & "9;AROLDO- SONIA;;??;??;??;??;??;??;??;??;12;1;335;70;12677;15;MDF BRANCO 15MM 2F. 2,75X1,85;C:\corte_certo_plus\PEDLABEL\PLA00010_00012.bmp; @B8  @T8  @L8 @R8 ;;BET;;;;;1;3;AROLDO;10;;;BET;;;;;AROLDO;;;;;;4227072;0;42;335;70;;;;-;;;;;;;;;;;;;;;;;12677;" & vbCrLf
        'Conteúdo = Conteúdo & "10;AROLDO- SONIA;;??;??;??;??;??;??;??;??;5;1;735;70;12677;15;MDF BRANCO 15MM 2F. 2,75X1,85;C:\corte_certo_plus\PEDLABEL\PLA00010_00005.bmp; @B8  @T8  ;;BET;;;;;1;3;AROLDO;10;;;BET;;;;;AROLDO;;;;;;4227072;0;42;735;70;;;;-;;;;;;;;;;;;;;;;;12677;" & vbCrLf
        'Conteúdo = Conteúdo & "11;AROLDO- SONIA;;??;??;??;??;??;??;??;??;13;1;336;70;12677;15;MDF BRANCO 15MM 2F. 2,75X1,85;C:\corte_certo_plus\PEDLABEL\PLA00010_00013.bmp; @B8  @T8  @L8 @R8 ;;BET;;;;;1;3;AROLDO;10;;;BET;;;;;AROLDO;;;;;;4227072;0;42;336;70;;;;-;;;;;;;;;;;;;;;;;12677;" & vbCrLf
        'Conteúdo = Conteúdo & "12;AROLDO- SONIA;;??;??;??;??;??;??;??;??;12;1;335;70;12677;15;MDF BRANCO 15MM 2F. 2,75X1,85;C:\corte_certo_plus\PEDLABEL\PLA00010_00012.bmp; @B8  @T8  @L8 @R8 ;;BET;;;;;1;3;AROLDO;10;;;BET;;;;;AROLDO;;;;;;4227072;0;42;335;70;;;;-;;;;;;;;;;;;;;;;;12677;" & vbCrLf
        'Conteúdo = Conteúdo & "13;AROLDO- SONIA;;??;??;??;??;??;??;??;??;5;1;735;70;12677;15;MDF BRANCO 15MM 2F. 2,75X1,85;C:\corte_certo_plus\PEDLABEL\PLA00010_00005.bmp; @B8  @T8  ;;BET;;;;;1;3;AROLDO;10;;;BET;;;;;AROLDO;;;;;;4227072;0;42;735;70;;;;-;;;;;;;;;;;;;;;;;12677;" & vbCrLf
        'Conteúdo = Conteúdo & "14;AROLDO- SONIA;;??;??;??;??;??;??;??;??;13;1;336;70;12677;15;MDF BRANCO 15MM 2F. 2,75X1,85;C:\corte_certo_plus\PEDLABEL\PLA00010_00013.bmp; @B8  @T8  @L8 @R8 ;;BET;;;;;1;3;AROLDO;10;;;BET;;;;;AROLDO;;;;;;4227072;0;42;336;70;;;;-;;;;;;;;;;;;;;;;;12677;" & vbCrLf
        'Conteúdo = Conteúdo & "15;AROLDO- SONIA;;??;??;??;??;??;??;??;??;12;1;335;70;12677;15;MDF BRANCO 15MM 2F. 2,75X1,85;C:\corte_certo_plus\PEDLABEL\PLA00010_00012.bmp; @B8  @T8  @L8 @R8 ;;BET;;;;;1;3;AROLDO;10;;;BET;;;;;AROLDO;;;;;;4227072;0;42;335;70;;;;-;;;;;;;;;;;;;;;;;12677;" & vbCrLf
        'Conteúdo = Conteúdo & "16;AROLDO- SONIA;;??;??;??;??;??;??;??;??;5;1;735;70;12677;15;MDF BRANCO 15MM 2F. 2,75X1,85;C:\corte_certo_plus\PEDLABEL\PLA00010_00005.bmp; @B8  @T8  ;;BET;;;;;1;3;AROLDO;10;;;BET;;;;;AROLDO;;;;;;4227072;0;42;735;70;;;;-;;;;;;;;;;;;;;;;;12677;" & vbCrLf
        'Conteúdo = Conteúdo & "17;AROLDO- SONIA;;??;??;??;??;??;??;??;??;13;1;336;70;12677;15;MDF BRANCO 15MM 2F. 2,75X1,85;C:\corte_certo_plus\PEDLABEL\PLA00010_00013.bmp; @B8  @T8  @L8 @R8 ;;BET;;;;;1;3;AROLDO;10;;;BET;;;;;AROLDO;;;;;;4227072;0;42;336;70;;;;-;;;;;;;;;;;;;;;;;12677;" & vbCrLf
        'Conteúdo = Conteúdo & "18;AROLDO- SONIA;;??;??;??;??;??;??;??;??;12;1;335;70;12677;15;MDF BRANCO 15MM 2F. 2,75X1,85;C:\corte_certo_plus\PEDLABEL\PLA00010_00012.bmp; @B8  @T8  @L8 @R8 ;;BET;;;;;1;3;AROLDO;10;;;BET;;;;;AROLDO;;;;;;4227072;0;42;335;70;;;;-;;;;;;;;;;;;;;;;;12677;" & vbCrLf
        'Conteúdo = Conteúdo & "19;AROLDO- SONIA;;??;??;??;??;??;??;??;??;5;1;735;70;12677;15;MDF BRANCO 15MM 2F. 2,75X1,85;C:\corte_certo_plus\PEDLABEL\PLA00010_00005.bmp; @B8  @T8  ;;BET;;;;;1;3;AROLDO;10;;;BET;;;;;AROLDO;;;;;;4227072;0;42;735;70;;;;-;;;;;;;;;;;;;;;;;12677;" & vbCrLf
        'Conteúdo = Conteúdo & "20;AROLDO- SONIA;;??;??;??;??;??;??;??;??;13;1;336;70;12677;15;MDF BRANCO 15MM 2F. 2,75X1,85;C:\corte_certo_plus\PEDLABEL\PLA00010_00013.bmp; @B8  @T8  @L8 @R8 ;;BET;;;;;1;3;AROLDO;10;;;BET;;;;;AROLDO;;;;;;4227072;0;42;336;70;;;;-;;;;;;;;;;;;;;;;;12677;" & vbCrLf
        'Conteúdo = Conteúdo & "21;AROLDO- SONIA;;??;??;??;??;??;??;??;??;12;1;335;70;12677;15;MDF BRANCO 15MM 2F. 2,75X1,85;C:\corte_certo_plus\PEDLABEL\PLA00010_00012.bmp; @B8  @T8  @L8 @R8 ;;BET;;;;;1;3;AROLDO;10;;;BET;;;;;AROLDO;;;;;;4227072;0;42;335;70;;;;-;;;;;;;;;;;;;;;;;12677;" & vbCrLf
        'Conteúdo = Conteúdo & "22;AROLDO- SONIA;;??;??;??;??;??;??;??;??;5;1;735;70;12677;15;MDF BRANCO 15MM 2F. 2,75X1,85;C:\corte_certo_plus\PEDLABEL\PLA00010_00005.bmp; @B8  @T8  ;;BET;;;;;1;3;AROLDO;10;;;BET;;;;;AROLDO;;;;;;4227072;0;42;735;70;;;;-;;;;;;;;;;;;;;;;;12677;" & vbCrLf
        'Conteúdo = Conteúdo & "23;AROLDO- SONIA;;??;??;??;??;??;??;??;??;13;1;336;70;12677;15;MDF BRANCO 15MM 2F. 2,75X1,85;C:\corte_certo_plus\PEDLABEL\PLA00010_00013.bmp; @B8  @T8  @L8 @R8 ;;BET;;;;;1;3;AROLDO;10;;;BET;;;;;AROLDO;;;;;;4227072;0;42;336;70;;;;-;;;;;;;;;;;;;;;;;12677;" & vbCrLf
        'Conteúdo = Conteúdo & "24;AROLDO- SONIA;;??;??;??;??;??;??;??;??;12;1;335;70;12677;15;MDF BRANCO 15MM 2F. 2,75X1,85;C:\corte_certo_plus\PEDLABEL\PLA00010_00012.bmp; @B8  @T8  @L8 @R8 ;;BET;;;;;1;3;AROLDO;10;;;BET;;;;;AROLDO;;;;;;4227072;0;42;335;70;;;;-;;;;;;;;;;;;;;;;;12677;" & vbCrLf
        'Conteúdo = Conteúdo & "25;AROLDO- SONIA;;??;??;??;??;??;??;??;??;5;1;735;70;12677;15;MDF BRANCO 15MM 2F. 2,75X1,85;C:\corte_certo_plus\PEDLABEL\PLA00010_00005.bmp; @B8  @T8  ;;BET;;;;;1;3;AROLDO;10;;;BET;;;;;AROLDO;;;;;;4227072;0;42;735;70;;;;-;;;;;;;;;;;;;;;;;12677;" & vbCrLf
        'Conteúdo = Conteúdo & "26;AROLDO- SONIA;;??;??;??;??;??;??;??;??;13;1;336;70;12677;15;MDF BRANCO 15MM 2F. 2,75X1,85;C:\corte_certo_plus\PEDLABEL\PLA00010_00013.bmp; @B8  @T8  @L8 @R8 ;;BET;;;;;1;3;AROLDO;10;;;BET;;;;;AROLDO;;;;;;4227072;0;42;336;70;;;;-;;;;;;;;;;;;;;;;;12677;" & vbCrLf
        'Conteúdo = Conteúdo & "27;AROLDO- SONIA;;??;??;??;??;??;??;??;??;12;1;335;70;12677;15;MDF BRANCO 15MM 2F. 2,75X1,85;C:\corte_certo_plus\PEDLABEL\PLA00010_00012.bmp; @B8  @T8  @L8 @R8 ;;BET;;;;;1;3;AROLDO;10;;;BET;;;;;AROLDO;;;;;;4227072;0;42;335;70;;;;-;;;;;;;;;;;;;;;;;12677;" & vbCrLf
        'Conteúdo = Conteúdo & "28;AROLDO- SONIA;;??;??;??;??;??;??;??;??;5;1;735;70;12677;15;MDF BRANCO 15MM 2F. 2,75X1,85;C:\corte_certo_plus\PEDLABEL\PLA00010_00005.bmp; @B8  @T8  ;;BET;;;;;1;3;AROLDO;10;;;BET;;;;;AROLDO;;;;;;4227072;0;42;735;70;;;;-;;;;;;;;;;;;;;;;;12677;" & vbCrLf
        'Conteúdo = Conteúdo & "29;AROLDO- SONIA;;??;??;??;??;??;??;??;??;13;1;336;70;12677;15;MDF BRANCO 15MM 2F. 2,75X1,85;C:\corte_certo_plus\PEDLABEL\PLA00010_00013.bmp; @B8  @T8  @L8 @R8 ;;BET;;;;;1;3;AROLDO;10;;;BET;;;;;AROLDO;;;;;;4227072;0;42;336;70;;;;-;;;;;;;;;;;;;;;;;12677;" & vbCrLf
        'Conteúdo = Conteúdo & "30;AROLDO- SONIA;;??;??;??;??;??;??;??;??;12;1;335;70;12677;15;MDF BRANCO 15MM 2F. 2,75X1,85;C:\corte_certo_plus\PEDLABEL\PLA00010_00012.bmp; @B8  @T8  @L8 @R8 ;;BET;;;;;1;3;AROLDO;10;;;BET;;;;;AROLDO;;;;;;4227072;0;42;335;70;;;;-;;;;;;;;;;;;;;;;;12677;" & vbCrLf
        'Conteúdo = Conteúdo & "31;AROLDO- SONIA;;??;??;??;??;??;??;??;??;5;1;735;70;12677;15;MDF BRANCO 15MM 2F. 2,75X1,85;C:\corte_certo_plus\PEDLABEL\PLA00010_00005.bmp; @B8  @T8  ;;BET;;;;;1;3;AROLDO;10;;;BET;;;;;AROLDO;;;;;;4227072;0;42;735;70;;;;-;;;;;;;;;;;;;;;;;12677;" & vbCrLf
        'Conteúdo = Conteúdo & "32;AROLDO- SONIA;;??;??;??;??;??;??;??;??;13;1;336;70;12677;15;MDF BRANCO 15MM 2F. 2,75X1,85;C:\corte_certo_plus\PEDLABEL\PLA00010_00013.bmp; @B8  @T8  @L8 @R8 ;;BET;;;;;1;3;AROLDO;10;;;BET;;;;;AROLDO;;;;;;4227072;0;42;336;70;;;;-;;;;;;;;;;;;;;;;;12677;" & vbCrLf
        'Conteúdo = Conteúdo & "33;AROLDO- SONIA;;??;??;??;??;??;??;??;??;12;1;335;70;12677;15;MDF BRANCO 15MM 2F. 2,75X1,85;C:\corte_certo_plus\PEDLABEL\PLA00010_00012.bmp; @B8  @T8  @L8 @R8 ;;BET;;;;;1;3;AROLDO;10;;;BET;;;;;AROLDO;;;;;;4227072;0;42;335;70;;;;-;;;;;;;;;;;;;;;;;12677;" & vbCrLf
        'Conteúdo = Conteúdo & "34;AROLDO- SONIA;;??;??;??;??;??;??;??;??;5;1;735;70;12677;15;MDF BRANCO 15MM 2F. 2,75X1,85;C:\corte_certo_plus\PEDLABEL\PLA00010_00005.bmp; @B8  @T8  ;;BET;;;;;1;3;AROLDO;10;;;BET;;;;;AROLDO;;;;;;4227072;0;42;735;70;;;;-;;;;;;;;;;;;;;;;;12677;" & vbCrLf
        ''Conteúdo = Conteúdo & "35;AROLDO- SONIA;;??;??;??;??;??;??;??;??;13;1;336;70;12677;15;MDF BRANCO 15MM 2F. 2,75X1,85;C:\corte_certo_plus\PEDLABEL\PLA00010_00013.bmp; @B8  @T8  @L8 @R8 ;;BET;;;;;1;3;AROLDO;10;;;BET;;;;;AROLDO;;;;;;4227072;0;42;336;70;;;;-;;;;;;;;;;;;;;;;;12677;" & vbCrLf
        ''Conteúdo = Conteúdo & "36;AROLDO- SONIA;;??;??;??;??;??;??;??;??;12;1;335;70;12677;15;MDF BRANCO 15MM 2F. 2,75X1,85;C:\corte_certo_plus\PEDLABEL\PLA00010_00012.bmp; @B8  @T8  @L8 @R8 ;;BET;;;;;1;3;AROLDO;10;;;BET;;;;;AROLDO;;;;;;4227072;0;42;335;70;;;;-;;;;;;;;;;;;;;;;;12677;" & vbCrLf
        ''Conteúdo = Conteúdo & "37;AROLDO- SONIA;;??;??;??;??;??;??;??;??;5;1;735;70;12677;15;MDF BRANCO 15MM 2F. 2,75X1,85;C:\corte_certo_plus\PEDLABEL\PLA00010_00005.bmp; @B8  @T8  ;;BET;;;;;1;3;AROLDO;10;;;BET;;;;;AROLDO;;;;;;4227072;0;42;735;70;;;;-;;;;;;;;;;;;;;;;;12677;" & vbCrLf
        ''Conteúdo = Conteúdo & "38;AROLDO- SONIA;;??;??;??;??;??;??;??;??;13;1;336;70;12677;15;MDF BRANCO 15MM 2F. 2,75X1,85;C:\corte_certo_plus\PEDLABEL\PLA00010_00013.bmp; @B8  @T8  @L8 @R8 ;;BET;;;;;1;3;AROLDO;10;;;BET;;;;;AROLDO;;;;;;4227072;0;42;336;70;;;;-;;;;;;;;;;;;;;;;;12677;" & vbCrLf
        ''Conteúdo = Conteúdo & "39;AROLDO- SONIA;;??;??;??;??;??;??;??;??;12;1;335;70;12677;15;MDF BRANCO 15MM 2F. 2,75X1,85;C:\corte_certo_plus\PEDLABEL\PLA00010_00012.bmp; @B8  @T8  @L8 @R8 ;;BET;;;;;1;3;AROLDO;10;;;BET;;;;;AROLDO;;;;;;4227072;0;42;335;70;;;;-;;;;;;;;;;;;;;;;;12677;" & vbCrLf
        Call GeraEtiquetas(Application.StartupPath & "\t2 - carta 6280.pdf", 4.8, 4.8, 12.7, 12.7, 10, 3, 66.7, 25.4, Conteúdo, 0, 5, 2, True, True)
        Call GeraEtiquetas(Application.StartupPath & "\t.pdf", 6.2, 6.2, 8.8, 8.8, 11, 3, 63.5, 25.4, Conteúdo, 0, 2.54, 1, True, True)
        'Call GeraEtiquetas("c:\T\t.pdf", 0.72, 0.72, 0.88, 0.88, 11, 3, 63.5, 25.4, Conteúdo, 2.54, 6.61)
    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim SplitaCommand As Object
        Dim ArmazenaArquivo As String
        If InStr(Command$, "/") > 0 Then
            Me.Hide()
            SplitaCommand = Split(Command$, "/")
            If System.IO.File.Exists(SplitaCommand(1)) = True Then
                If System.IO.Directory.Exists(System.IO.Path.GetDirectoryName(SplitaCommand(2))) = True Then
                    ArmazenaArquivo = System.IO.File.ReadAllText(SplitaCommand(1), System.Text.Encoding.Default)
                    'Código Pimaco              Margem Superior   Margem Lateral Densidade Vertical    Densidade horizontal	    Altura da Etiqueta    Largura da Etiqueta    Etiqueta por Linha   Linha por  Página
                    'A4056/A4256/A4356/A4056R	0,88	          0,72	          2,54	                6,61	                2,54	               6,35	                 3	                  11
                    Call GeraEtiquetas(SplitaCommand(2), 0.72, 0.72, 0.88, 0.88, 11, 3, 63.5, 25.4, ArmazenaArquivo, 2.54, 6.61, 1, False, False)
                Else
                    System.IO.File.AppendAllText(Application.StartupPath & "\LogEtiq.Log", DateTime.Now.ToString("dd/MM/yyyy") & " - " & DateTime.Now.ToString("HH:mm:ss") & " - Erro na geração de etiquetas - diretório de etiquetas inexistente" & vbCrLf)
                End If
            Else
                System.IO.File.AppendAllText(Application.StartupPath & "\LogEtiq.Log", DateTime.Now.ToString("dd/MM/yyyy") & " - " & DateTime.Now.ToString("HH:mm:ss") & " - Erro na geração de etiquetas - arquivo do Bnest inexistente" & vbCrLf)
            End If
            Me.Dispose()
            Application.Exit()
        End If
    End Sub
End Class
